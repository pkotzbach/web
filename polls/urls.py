from django.urls import path

from . import views

urlpatterns = [
    # ex: /polls/
    path("", views.index, name="index"),
    # ex: /polls/movie/5/
    path("movie/<int:movie_id>/", views.detail, name="detail"),
    # ex: /polls/showing/5/
    path("showing/<int:showing_id>/", views.seats, name="seats"),
    # ex: /polls/showing/5/
    path("showing/<int:showing_id>/<int:seat_id>", views.bookSeat, name="book seat"),
]
