from tokenize import String
from django.db import models

# TODO: zmienic z kaskadowego usuwania
# TODO: validatory?
# TODO: ogarnac __str__


class Movie(models.Model):
    title = models.CharField(max_length=200)
    description = models.CharField(max_length=1000)
    photo = models.BinaryField()

    def __str__(self):
        return self.title


class Room(models.Model):
    rows = models.IntegerField(default=10)
    spaceInRow = models.IntegerField(default=20)
    pattern = models.CharField(default="", max_length=200)


class Showing(models.Model):
    movie = models.ForeignKey(Movie, on_delete=models.CASCADE)
    room = models.ForeignKey(Room, on_delete=models.CASCADE)
    date = models.DateTimeField()

    def __str__(self):
        return self.date.strftime("%m/%d/%Y, %H:%M:%S")


class Client(models.Model):
    email = models.CharField(max_length=200)

    def __str__(self):
        return self.email


class Seat(models.Model):
    showing = models.ForeignKey(Showing, on_delete=models.CASCADE)
    client = models.ForeignKey(Client, on_delete=models.CASCADE, blank=True, null=True)
    row = models.IntegerField(default=-1)
    number = models.IntegerField(default=-1)
    seatType = models.IntegerField(default=0)

    def __str__(self):
        return str(self.row) + ": " + str(self.number)
