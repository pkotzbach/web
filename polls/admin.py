from django.contrib import admin

from .models import *


class ShowingAdmin(admin.ModelAdmin):
    def save_model(self, request, obj, form, change):
        obj.save()

        # add seats for showing
        for row in range(obj.room.rows):
            for seat in range(obj.room.spaceInRow):
                pattern = obj.room.pattern[row * obj.room.spaceInRow + seat]
                Seat.objects.create(
                    showing=obj, row=row + 1, number=seat + 1, seatType=int(pattern)
                )


# Register your models here.
admin.site.register(Showing, ShowingAdmin)
admin.site.register(Movie)
admin.site.register(Room)
admin.site.register(Seat)
