from django.http import HttpResponse
from .models import Movie, Showing, Seat
from django.shortcuts import render


def index(request):
    movies_list = Movie.objects.order_by("-title")[:5]
    context = {"movies_list": movies_list}
    return render(request, "polls/index.html", context)


def detail(request, movie_id):
    # TODO: fixme
    movie = Movie.objects.get(id=movie_id)
    context = {"movie": movie}
    return render(request, "polls/details.html", context)


def seats(request, showing_id):
    seats = Seat.objects.filter(showing=showing_id).order_by("row")
    if request.method == "POST":
        for line in request.POST.getlist("seats"):
            numbers = line.split(" ")
            print(int(numbers[0]), int(numbers[1]))
    context = {"seats": seats, "request": request}
    return render(request, "polls/seats.html", context)


def bookSeat(request, showing_id, seat_id):
    context = {}
    return render(request, "polls/bookSeat.html", context)
